module.exports = async (page) => {
  try {
    const description = await page.$eval('.catalog .desc .text', div => div.innerHTML)

    return description
  } catch (err) {
    return null
  }
}
