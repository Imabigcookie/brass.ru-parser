const fs = require('fs');
const axios = require('axios');
const path = require('path')

downloadImage = async (link, index, productCode) => {
  const url = link
  const fileExt = path.extname(link)
  const filePath = path.resolve(`documents/${productCode}/images`, `image_${index}.${fileExt}`)
  const writer = fs.createWriteStream(filePath)

  const response = await axios({
    url,
    method: 'GET',
    responseType: 'stream',
  })

  response.data.pipe(writer)

  return new Promise((res, rej) => {
    writer.on('finish', res)
    writer.on('error', rej)
  })
}

module.exports = async (page, productName) => {
  const downloadLinksArray = await page.$$eval('.catalog .gal .big img', images => images.map(image => image.src))
  const correctProductName = productName.replace(/\//g, '+').replace(/\*/g, '~').replace(/"/g, '')
  const dirPath = path.resolve(`documents/${correctProductName}/images`)

  fs.mkdirSync(dirPath, { recursive: true })

  await Promise.all(downloadLinksArray.map((link, index) => downloadImage(link, index + 1, correctProductName)))
}
