module.exports = async (page) => {
  let allCharacteristics
  const characteristicKeys = await page.$$eval('.catalog .desc .tech tr > .l', keys => keys.map(key => key.innerText))
  const characteristicValues = await page.$$eval(
    '.catalog .desc .tech tr > .r',
    values => values.map(value => value.innerText)
  )

  allCharacteristics = characteristicKeys.reduce((acc, key, index) => {
    acc[key] = characteristicValues[index]

    return acc
  }, {})

  return allCharacteristics
}
