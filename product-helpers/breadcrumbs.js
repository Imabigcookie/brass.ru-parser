module.exports = async (page) => {
  const allBreadcrumbs = await page.$$eval('.navy_block .pad > .item > a', links => links.map(link => link.innerText))

  allBreadcrumbs.splice(0, 2)

  return allBreadcrumbs
}
