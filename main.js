const puppeteer = require('puppeteer');
const catalogParser = require('./parse-category');
const clearProject = require('./clear-project');

(async () => {
  await clearProject()

  const browser = await puppeteer.launch()
  const page = await browser.newPage()

  await page.goto('https://www.brass.ru/catalog/225/')

  const catalogLinks = await page.$$eval(
    '.navCatalog tr > td a',
    links => links.map(link => `${link.href}?SHOWALL_1=1`)
  )

  for (const catalogLink of catalogLinks) {
    await catalogParser(page, catalogLink)
  }

  await browser.close()
  console.log('Done!')
})();
