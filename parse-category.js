const parseProductInfo = require('./parse-product-info')

parseCategory = async (page, link) => {
  await page.goto(link)

  const productLinks = await page.$$eval('.catalog > tbody > tr > td.item > .name a', links => links.map(link => link.href))

  if (!productLinks.length) {
    try {
      const catalogLinks = await page.$$eval(
        '.navCatalog tr > td a',
        links => links.map(link => `${link.href}?SHOWALL_1=1`)
      )

      for (const catalogLink of catalogLinks) {
        await parseCategory(page, catalogLink)
      }

    } catch (err) {
    }

    return
  }

  for (const productLink of productLinks) {
    await parseProductInfo(page, productLink)
  }

  console.log('Category parsed!')
}

module.exports = parseCategory
